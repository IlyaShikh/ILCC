from ILCC import utility
import numpy as np
import sys

if len(sys.argv) != 3:
    exit("Please, write only 2 numbers, which is the first and last pcd numbers!!!")
for i in range(1,len(sys.argv)):
    arg = str(sys.argv[i])
    if not arg.isdigit():
        exit("Argument {:d} is not an unsigned number!!".format(i))
f_number = int(sys.argv[1])
l_number = int(sys.argv[2])
a =1
utility.vis_all_markers(utility.vis_all_markers(np.arange(f_number, l_number).tolist()))

#cmake ../opengv -DBUILD_PYTHON=ON -DPYBIND11_PYTHON_VERSION=3.6 -DPYTHON_INSTALL_DIR=/usr/local/lib/python3.6/dist-packages/